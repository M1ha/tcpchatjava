package com.mycompany.app;

import java.util.Arrays;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;

import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import com.mycompany.app.server.Server;
import com.mycompany.app.client.Client;
import com.mycompany.app.bot.ChatBot;
import com.mycompany.app.bot.BotFather;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Main application class
 */
public class Main {

    /**
     * Checks, that if parameter baseParam is declared, there can be only relativeParams present, not any other
     *
     * @param cmdLine        CommandLine to get parameters from
     * @param baseParam      Name of base parameter
     * @param relativeParams Name of relativeParameters
     * @return True, if check passed. False if wrong parameters found
     */
    public static boolean checkParameters(CommandLine cmdLine, String baseParam, String[] relativeParams) {
        if (cmdLine.hasOption(baseParam)) {
            for (Option opt : cmdLine.getOptions()) {
                String optName = opt.getLongOpt() == null ? opt.getOpt() : opt.getLongOpt();
                if (!Arrays.asList(relativeParams).contains(optName) && !optName.equals(baseParam)) {
                    System.err.print("You can't use --" + optName + " with --" + baseParam + " option\n");
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * Main application function
     *
     * @param args Arguments, passed in command string
     * @throws CloneNotSupportedException     If Clone is not supported in the system
     * @throws InterruptedException           If main process was interrupted
     * @throws CommandNotInitializedException If some command during client cycle was not initialized properly
     * @throws IOException                    If any stream error in bot occures
     * @throws URISyntaxException             If there is an error in current file path
     */
    public static void main(String[] args)
            throws CloneNotSupportedException, InterruptedException, CommandNotInitializedException, IOException,
            URISyntaxException {

        Options options = new Options();
        options.addOption(new Option("H", "help", false, "Start TcpChat client"));
        options.addOption(new Option("S", "server", false, "Start TcpChat listening server"));
        options.addOption(new Option("C", "client", false, "Start TcpChat client"));
        options.addOption(new Option("C", "bot", false, "Start Chat bot"));
        options.addOption(new Option("C", "botFather", false, "Start Bot father, who start bots"));

        Option jarDirOpt = OptionBuilder.withArgName("path").hasArg()
                .withDescription("Bot and BotFather executable jar (this jar). Searches in current dir by default")
                .create("jarPath");
        options.addOption(jarDirOpt);

        Option sleepTimeOpt = OptionBuilder.withArgName("integer").hasArg()
                .withDescription("Time in miliseconds for bot to sleep between sending messages. Defaults to 1 second.")
                .create("botSleepTime");
        options.addOption(sleepTimeOpt);

        Option botNumberOpt = OptionBuilder.withArgName("integer").hasArg()
                .withDescription("Time in miliseconds for bot to sleep between sending messages. Defaults to 1 second.")
                .create("botNumber");
        options.addOption(botNumberOpt);

        Option hostOptionOpt = OptionBuilder.withArgName("ip").hasArg()
                .withDescription("TcpChat server host or ALL for all ips. Defaults to '127.0.0.1'.").create("host");
        options.addOption(hostOptionOpt);
        Option portOptionOpt = OptionBuilder.withArgName("integer").hasArg()
                .withDescription("TcpChat server port. Defaults to 54321.").withType(Integer.class).create("port");
        options.addOption(portOptionOpt);

        CommandLineParser parser = new BasicParser();

        try {
            CommandLine l = parser.parse(options, args);
            String[] optList = new String[5];
            if (!checkParameters(l, "help", optList))
                return;
            if (!checkParameters(l, "client", optList))
                return;
            optList[0] = "host";
            optList[1] = "port";
            if (!checkParameters(l, "server", optList))
                return;
            optList[2] = "jarPath";
            optList[3] = "botSleepTime";
            if (!checkParameters(l, "bot", optList))
                return;
            optList[4] = "botNumber";
            if (!checkParameters(l, "botFather", optList))
                return;

            if (l.hasOption("server") || l.hasOption("bot") || l.hasOption("botFather")) {
                String host = l.hasOption("host") ? l.getOptionValue("host") : "127.0.0.1";
                Integer port = l.hasOption("port") ? new Integer(l.getOptionValue("port")) : 54321;
                if (l.hasOption("server")) {
                    int historySize = l.hasOption("history-size") ? new Integer(l.getOptionValue("port")) : 100;
                    // Start server
                    new Server(host, port, historySize);
                } else {
                    String jarPath = l.hasOption("jarPath") ? l.getOptionValue("jarPath") : "TcpChat-1.0-jar-with-dependencies.jar";
                    String[] subProcArgs = {"-jar", "\"" + jarPath + "\""};
                    Integer sleepTime = l.hasOption("botSleepTime") ? new Integer(l.getOptionValue("botSleepTime")) : 1000;
                    if (l.hasOption("bot")) {
                        ChatBot b = new ChatBot("java", subProcArgs, sleepTime);
                        b.connect(host, port);
                        // In order to finish connection
                        b.waitForConsoleReading();
                        b.runInteractionStringCycle();
                    } else if (l.hasOption("botFather")) {
                        Integer botNumber = l.hasOption("botNumber") ? new Integer(l.getOptionValue("botNumber")) : 1000;
                        new BotFather(host, port, "java", subProcArgs, botNumber, sleepTime);
                    }
                }
            } else if (l.hasOption("help")) {
                HelpFormatter helpForm = new HelpFormatter();
                helpForm.printHelp("ant", options);
            } else if (l.getOptions().length == 0 || l.hasOption("client")){
                // Start client
                System.out.print("Enter /help command to see manual.\n");
                Client cl = new Client();
                cl.runInputCycle();
            } else
                System.err.print("Invalid options for default --client option.\n");
        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Error in arguments: " + exp.getMessage() + "\n");
        } catch (NumberFormatException exp) {
            // oops, something went wrong
            System.err.println("Error in arguments: post must be an integer.\n");
        }
    }
}
