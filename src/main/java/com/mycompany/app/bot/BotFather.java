package com.mycompany.app.bot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class creates a number of bots, "speaking" to each other
 */
public class BotFather {
    private Thread[] threads;


    public BotFather(String serverHost, int serverPort, String command, String[] commandArgs, int botNumber,
                     int sleepTime) throws IOException, InterruptedException {
        this.threads = new Thread[botNumber];
        for (int i = 0; i < botNumber; i++) {
            ArrayList<String> cmd = new ArrayList<String>();
            cmd.add(command);
            cmd.addAll(Arrays.asList(commandArgs));
            cmd.add("--bot");
            cmd.add("--sleepTime");
            cmd.add(Integer.toString(sleepTime));
            cmd.add("--host");
            cmd.add(serverHost);
            cmd.add("--port");
            cmd.add(Integer.toString(serverPort));
            ChatBot b = new ChatBot("java", commandArgs, sleepTime);
            b.connect(serverHost, serverPort);
            // In order to finish connection
            b.waitForConsoleReading();
            this.threads[i] = new BotFatherThread(b);
            this.threads[i].setDaemon(true);
            this.threads[i].setPriority(Thread.NORM_PRIORITY);
            this.threads[i].start();
        }

        for (int i = 0; i < botNumber; i++)
            this.threads[i].join();
    }
}
