package com.mycompany.app.bot;

import java.io.IOException;
import java.util.Stack;

public class BotFatherThread extends Thread {
    private ChatBot bot;


    /**
     * Runs a bot in subthread
     *
     * @param bot Bot to work with
     */
    public BotFatherThread(ChatBot bot) {
        this.bot = bot;
    }


    public void run() {
        try {
            if (this.bot.isConnectedToServer()) {
                System.out.print("Bot connected\n");
                this.bot.runInteractionStringCycle();
            }
            System.err.print("Bot " + Thread.currentThread().getId() + "terminated without exception!\n");
        } catch (Exception e) {
            // finish thread as error
            System.err.print("Bot " + Thread.currentThread().getId() + "terminated with exception " +
                    e.getLocalizedMessage() + ":\n");
            for (StackTraceElement s :e.getStackTrace())
                System.err.print(s.toString() + "\n");
        }
    }
}