package com.mycompany.app.bot;

import java.io.IOException;
import java.util.Random;
import java.util.Arrays;
import java.util.ArrayList;

import com.mycompany.app.client.Client;
import com.mycompany.app.client.commands.SendMessageCommand;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;


/**
 * This class creates a chat bot which writes command to chat client and gets result to stdOutput
 */
public class ChatBot extends Client {
    protected int sleepTime;


    /**
     * Initializes new client process
     *
     * @param command   Command to execute ("java")
     * @param args      Arguments for command ("-jar", "filename", "chat params")
     * @param sleepTime Time in miliseconds to sleep between sending messages
     * @throws IOException If server connection fails
     */
    public ChatBot(String command, String[] args, int sleepTime) throws IOException {
        super();
        ArrayList<String> cmd = new ArrayList<String>();
        cmd.add(command);
        cmd.addAll(Arrays.asList(args));
        ProcessBuilder pb = new ProcessBuilder(cmd);
        pb.redirectErrorStream(true);
        this.sleepTime = sleepTime;
    }


    /**
     * Generates new random string
     *
     * @param characters A list of characters to get from
     * @param length     Length of result string
     * @return Random string
     */
    public static String generateString(String characters, int length) {
        Random rng = new Random();
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        return new String(text);
    }


    /**
     * Starts cycle, which sends random string to chat every second
     *
     * @throws IOException          If any stream error
     * @throws InterruptedException If sleep was interrupted
     */
    public void runInteractionStringCycle() throws IOException, InterruptedException, CommandNotInitializedException {
        if (this.isConnectedToServer()) {
            Random length = new Random();
            String alphaNumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            SendMessageCommand sendCmd = new SendMessageCommand();
            while (true) {
                sendCmd.fromRawData(this.name, "Sys", ChatBot.generateString(alphaNumeric, length.nextInt(128)));
                sendCmd.execute(this);
                Thread.sleep(this.sleepTime);
            }
        }
    }



    public void printMessage(String msg, String from) {
        //System.out.print(from + ": " + msg + "\n");
        // Ignores correct output
    }
}
