package com.mycompany.app.client;

import java.io.OutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

import com.mycompany.app.commands.AbstractCommand;
import com.mycompany.app.commands.CommandFactory;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;


/**
 * TcpChat client main class.
 */
public class Client {
    protected CommandFactory sysCmdFactory, clientCmdFactory;
    protected String name;
    protected Thread readThread;
    protected Socket sock;
    protected boolean connected;
    private boolean readingStopped;


    /**
     * Initializes class fields
     */
    public Client() {
        String[] scopeList = {"sys"};
        this.sysCmdFactory = new CommandFactory("com.mycompany.app.client.commands", scopeList);
        scopeList[0] = "client";
        this.clientCmdFactory = new CommandFactory("com.mycompany.app.client.commands", scopeList);
        this.name = "Client";
        this.sock = null;
        this.readingStopped = false;
    }


    /**
     * Initializes client, setting his name
     *
     * @param name Client name string
     */
    public Client(String name) {
        String[] scopeList = {"sys"};
        this.sysCmdFactory = new CommandFactory("com.mycompany.app.client.commands", scopeList);
        scopeList[0] = "client";
        this.clientCmdFactory = new CommandFactory("com.mycompany.app.client.commands", scopeList);
        this.name = name;
        this.sock = null;
        this.readingStopped = false;
    }


    /**
     * Sends command to server, in order to execute it there.
     *
     * @param cmd AbstractCommand child instance to send to server
     * @throws CommandNotInitializedException If command was not initialized properly.
     */
    public void sendCommandToServer(AbstractCommand cmd) throws CommandNotInitializedException {
        // Send to server cmd.toJSON();
        if (this.isConnectedToServer()) {
            try {
                OutputStream outStream = this.sock.getOutputStream();
                outStream.write((cmd.toJSONString() + "\0").getBytes());
            } catch (IOException e) {
                this.printError("Error writing to socket", "Sys");
            }
        } else
            this.printError("You are not connected to server. Please, use /connect command", "Sys");
    }


    /**
     * Outputs correct message.
     * All methods, who make any output for client use this method.
     *
     * @param msg  Message to output for client
     * @param from Message sender, to output "Name:" prefix
     */
    public void printMessage(String msg, String from) {
        System.out.print(from + ": " + msg + "\n");
    }


    /**
     * Outputs error message.
     * All methods, who make any error output for client use this method.
     *
     * @param msg  Error message to output for client
     * @param from Message sender, to output "Name:" prefix
     */
    public void printError(String msg, String from) {
        System.err.print(from + ": " + msg + "\n");
    }


    /**
     * Tries connecting client to server.
     * If client was already connected, closes previous connection.
     *
     * @param host Ip address or host name to connect to
     * @param port Tcp port to connect to
     */
    public void connect(String host, int port) {
        try {
            this.stopConsoleReading();
            this.sock = new Socket(host, port);
            this.connected = true;
            this.readThread = new ClientReadThread(this, this.sock);
            this.readThread.setDaemon(true);
            this.readThread.setPriority(Thread.NORM_PRIORITY);
            this.readThread.start();
        } catch (Exception e) {
            this.printError("Connection error: " + e, "Sys");
            this.resumeConsoleReading();

        }
    }


    /**
     * Processes command string, entered by client
     *
     * @param command Command string to execute
     * @throws CloneNotSupportedException     If system doesn't support objects cloning.
     * @throws CommandNotInitializedException If command was not initialized properly
     */
    public void executeClientCommand(String command)
            throws CloneNotSupportedException, CommandNotInitializedException, InterruptedException {
        try {
            AbstractCommand cmd = this.clientCmdFactory.getCommand(command, this.name);
            cmd.execute(this);
        } catch (CommandNameException e) {
            this.printError("Unrecognized command: " + command, "Sys");
        }
    }


    /**
     * Returns initialized CommandFactory with scope Sys of current Client
     *
     * @return CommandFactory instance
     */
    public CommandFactory getSysCommandFactory() {
        return this.sysCmdFactory;
    }


    /**
     * Sets clients name. If client is connected to server, makes requests to change name on the server.
     * Otherwise - changes locally.
     *
     * @param name new name to set.
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Returns client name.
     *
     * @return Client name string.
     */
    public String getName() {
        return this.name;
    }


    /**
     * Forms help message, using data from client's scope CommandFactory
     *
     * @return A string to output as help message
     */
    public String getHelpString() {
        String result = "Hello! You can send me commands in format:\n";
        result += "[@receiver[, receiver]*] [command] [parameters]\n";
        result += " * receiver is a client name, connected to server or 'Sys' to ask system for something. ";
        result += "It is 'Sys' by default.\n";
        result += " * command is a value from the list below, starting with / symbol. It is /send by default.\n";
        result += " * parameters - everything after command. If they are not useful for command - they are ignored.\n\n";
        result += "Command list:\n";

        for (AbstractCommand cmd : this.clientCmdFactory.getCommandList()) {
            result += " /" + cmd.getName();
            if (cmd.getParamsDescription().length() > 0)
                result += ' ' + cmd.getParamsDescription();
            result += " - " + cmd.getDescription() + "\n";
        }

        return result;
    }


    /**
     * Checks if client is connected to any server at the moment
     *
     * @return Boolean
     */
    public boolean isConnectedToServer() {
        return connected && (this.sock != null);
    }


    /**
     * Marks, that client was successfully connected to server.
     * This method is called by ConnectSuccessCommand, received from server. So, it must be public.
     */
    public void setConnected() {
        if (this.sock != null)
            this.connected = true;
    }


    /**
     * Disconnects client from server
     * Read threads will finish automatically, when socket is closed.
     */
    public void disconnect() {
        if (this.isConnectedToServer()) {
            try {
                this.sock.close();
            } catch (IOException e) {
                this.printError("Close previous connection error: " + e.toString(), "Sys");
            }
            this.resumeConsoleReading();
            this.sock = null;
            this.connected = false;
        }
    }


    /**
     * Waits until all threads will give permission to read info from console.
     * Helps to realize difficult client-server commands without sending other commands.
     * For example, when client connects to server, he could already set his name by SetClientNameCommand.
     * He connects to server by ConnectCommand, it returns a new auto-generated name in ConnectSuccessCommand.
     * Then client has to update name on the server with SetClientNameCommand and receive SetClientNameSuccessCommand.
     * If client sends any command during this process the command can be rejected,
     * as names on client ans server can be different.
     *
     * @throws InterruptedException If wait was interrupted by something
     */
    public synchronized void waitForConsoleReading() throws InterruptedException {
        if (this.readingStopped)
            this.wait();
    }


    /**
     * Prevents client from sending new commands
     *
     * @throws InterruptedException If wait was interrupted by something
     */
    public synchronized void stopConsoleReading() throws InterruptedException {
        this.readingStopped = true;
    }


    /**
     * Returns client opportunity to send commands
     */
    public synchronized void resumeConsoleReading() {
        this.readingStopped = false;
        this.notifyAll();
    }


    /**
     * Read commands from console and execution cycle
     *
     * @throws CloneNotSupportedException     If Clone is not supported in system.
     * @throws CommandNotInitializedException If Command was not initialized properly, but executed
     * @throws InterruptedException           If Lock was interrupted
     */
    public void runInputCycle() throws CloneNotSupportedException, CommandNotInitializedException, InterruptedException {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                this.waitForConsoleReading();
                String line = scanner.nextLine();
                this.resumeConsoleReading();

                AbstractCommand cmd = this.clientCmdFactory.getCommand(line, this.name);
                cmd.execute(this);
            } catch (CommandNameException e) {
                this.printError("Unrecognized command", "Sys");
            }
        }
    }
}
