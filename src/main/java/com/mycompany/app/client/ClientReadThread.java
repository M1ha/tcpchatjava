package com.mycompany.app.client;

import com.mycompany.app.commands.AbstractCommand;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.SocketException;

/**
 * A thread, that reads commands from socket and executes them.
 */
public class ClientReadThread extends Thread {

    protected Client client;
    private Socket sock;
    private String readBuffer;


    /**
     * Initializes thread
     *
     * @param cl Client this thread is attached to
     * @param s  Socket this thread will read from
     */
    public ClientReadThread(Client cl, Socket s) {
        this.sock = s;
        this.client = cl;
        this.readBuffer = "";
    }


    public String readStringFromSocket() throws IOException {
        InputStream inpStream = this.sock.getInputStream();
        int splitIndex = -1;
        while (splitIndex < 0) {
            byte buf[] = new byte[64 * 1024];
            int readCnt = inpStream.read(buf);
            this.readBuffer += new String(buf, 0, readCnt);
            splitIndex = this.readBuffer.indexOf('\0');
        }
        String data = this.readBuffer.substring(0, splitIndex);
        this.readBuffer = this.readBuffer.substring(splitIndex + 1);
        return data;
    }

    /**
     * Thread main function.
     * Reads command from socket, tries parsing it. If succeeds, executes command.
     * If connection falls, marks client as disconnected and prints error.
     * If any other exception is raised, outputs it to error message and continues working.
     */
    public void run() {
        while (this.client.isConnectedToServer()) {
            try {
                String data = this.readStringFromSocket();

                // Convert data to command
                AbstractCommand cmd = this.client.getSysCommandFactory().getCommandFromJSONString(data);

                // Execute command
                cmd.execute(this.client);
            } catch (SocketException e) {
                if (this.client.isConnectedToServer()) {
                    this.client.disconnect();
                    this.client.printError("Server is down. You are disconnected.", "Sys");
                }
            } catch (Exception e) {
                this.client.printError(e.toString(), "Sys");
                for (StackTraceElement el : e.getStackTrace())
                    this.client.printError(el.toString(), "Sys");
            }
        }
    }
}
