package com.mycompany.app.client.commands;

import java.util.List;

import com.mycompany.app.commands.AbstractCommand;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

import java.util.Arrays;


/**
 * An abstract class of command, which can be requested by client (scope is client).
 * Default executor simply proxies command to server as is.
 */
public abstract class ClientAbstractCommand extends AbstractCommand {

    public ClientAbstractCommand() {
        super();
    }


    public ClientAbstractCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public ClientAbstractCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Simply sends this command to server
     *
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        Client cl = (Client) sender;
        cl.sendCommandToServer(this);
    }


    /**
     * Sends local command result.
     * If receiver (this.to) is Sys only, prints message for client only.
     * Otherwise sends local command result to receivers
     *
     * @param cl   Client to send message through
     * @param data Data string to send
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     */
    protected void sendLocalString(Client cl, String data) throws CommandNotInitializedException {
        List<String> receivers = Arrays.asList(this.to);
        if (receivers.contains("Sys"))
            cl.printMessage(data, "Sys");
        receivers.remove("Sys");
        if (receivers.size() > 0) {
            SendMessageCommand cmd = new SendMessageCommand();
            cmd.fromRawData(this.from, receivers.toArray(new String[0]), data);
            cl.sendCommandToServer(cmd);
        }
    }


    public String getScope() {
        return "client";
    }


    public String getParamsDescription() {
        return "";
    }
}
