package com.mycompany.app.client.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

/**
 * Connect to server command
 */
public class ConnectCommand extends ClientAbstractCommand {

    public ConnectCommand() {
        super();
    }


    public ConnectCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public ConnectCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Tries connecting to server. Validates host and port from input.
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Connect to server here
        Client cl = (Client) sender;
        cl.stopConsoleReading();
        if (cl.isConnectedToServer())
            cl.disconnect();
        String ipData = this.text.trim().split("\\s")[0];
        String[] splData = ipData.split(":");
        if (splData.length != 2) {
            cl.printError("You must specify host and port parameters like /connect 127.0.0.1:12345", "Sys");
            cl.resumeConsoleReading();
        } else {
            String ip = splData[0].trim();
            try {
                int port = new Integer(splData[1].trim());
                cl.connect(ip, port);
            } catch (NumberFormatException e) {
                cl.printError("Port must be an integer number like /connect 127.0.0.1:12345", "Sys");
                cl.resumeConsoleReading();
            }
        }
    }


    public String getName() {
        return "connect";
    }


    public String getDescription() {
        return "Connects to chat server";
    }


    public String getParamsDescription() {
        return "ip:port";
    }
}
