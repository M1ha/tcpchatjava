package com.mycompany.app.client.commands;

import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

/**
 * This command is sent by server, when successfully connected.
 * Text contains client name on server to communicate by.
 */
public class ConnectSuccessCommand extends SysAbstractCommand {
    public ConnectSuccessCommand() {
        super();
    }


    public ConnectSuccessCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public ConnectSuccessCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Checks, if client name must be changed. If needed, executes change command.
     * Otherwise resumes client console reading.
     *
     * @param sender A Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        Client cl = (Client) sender;
        cl.setConnected();
        if (!cl.getName().equals("Client")) {
            SetClientNameCommand cmd = new SetClientNameCommand();
            cmd.fromRawData(this.text, "Sys", cl.getName());
            cmd.execute(cl);
            // Reading would be resumed after SetClientNameSuccessCommand
        } else {
            cl.setName(this.text);
            cl.resumeConsoleReading();
        }
        cl.printMessage("Connect successful!", "Sys");
    }


    public String getName() {
        return "connectSuccess";
    }


    public String getDescription() {
        return "First command after successful connect";
    }


    public String getParamsDescription() {
        return "";
    }
}
