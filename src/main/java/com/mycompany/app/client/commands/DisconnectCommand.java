package com.mycompany.app.client.commands;

import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

/**
 * Connect to server command
 */
public class DisconnectCommand extends ClientAbstractCommand {

    public DisconnectCommand() {
        super();
    }


    public DisconnectCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public DisconnectCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Disconnects client from server, if he was connected
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Connect to server here
        Client cl = (Client) sender;
        if (cl.isConnectedToServer()) {
            cl.disconnect();
            cl.printMessage("You are disconnected from server", "Sys");
        }
        else
            cl.printError("You are not connected to server", "Sys");
    }


    public String getName() {
        return "disconnect";
    }


    public String getDescription() {
        return "Disconnects client from chat server";
    }
}
