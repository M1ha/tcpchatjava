package com.mycompany.app.client.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import org.json.simple.JSONObject;

/**
 * Gets number of clients, connected to server
 */
public class GetClientCountCommand extends ClientAbstractCommand {

    public GetClientCountCommand() {
        super();
    }


    public GetClientCountCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public GetClientCountCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    public String getName() {
        return "clientsCount";
    }


    public String getDescription() {
        return "Returns number of clients, connected to server";
    }
}
