package com.mycompany.app.client.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import org.json.simple.JSONObject;

/**
 * Gets list of client names
 */
public class GetClientListCommand extends ClientAbstractCommand {

    public GetClientListCommand() {
        super();
    }


    public GetClientListCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public GetClientListCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    public String getName() {
        return "clients";
    }


    public String getDescription() {
        return "Returns a list of client names, connected to server";
    }
}
