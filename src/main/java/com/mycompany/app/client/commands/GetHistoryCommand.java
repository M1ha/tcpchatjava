package com.mycompany.app.client.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import org.json.simple.JSONObject;

/**
 * Prints history of commands done by the user
 */
public class GetHistoryCommand extends ClientAbstractCommand {

    public GetHistoryCommand() {
        super();
    }


    public GetHistoryCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public GetHistoryCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    public String getName() {
        return "history";
    }


    public String getDescription() {
        return "Returns command history for current client";
    }


    public String getParamsDescription() {
        return "size";
    }
}
