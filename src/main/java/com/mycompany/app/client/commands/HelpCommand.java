package com.mycompany.app.client.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

/**
 * Shows help message
 */
public class HelpCommand extends ClientAbstractCommand {

    public HelpCommand() {
        super();
    }


    public HelpCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public HelpCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Shows help message
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Print help here
        Client cl = (Client) sender;
        this.sendLocalString(cl, cl.getHelpString());
    }


    public String getName() {
        return "help";
    }


    public String getDescription() {
        return "Returns help manual";
    }
}
