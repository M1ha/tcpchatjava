package com.mycompany.app.client.commands;

import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

import java.util.Date;

/**
 * Prints to client his local time string
 */
public class LocalTimeCommand extends ClientAbstractCommand {

    public LocalTimeCommand() {
        super();
    }


    public LocalTimeCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public LocalTimeCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Prints to client his local time
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Print local time
        Client cl = (Client) sender;
        Date now = new Date();
        this.sendLocalString(cl, now.toString());
    }


    public String getName() {
        return "time";
    }


    public String getDescription() {
        return "Returns your local time";
    }
}
