package com.mycompany.app.client.commands;

import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

/**
 * Prints message to client
 */
public class PrintMessageCommand extends SysAbstractCommand {

    public PrintMessageCommand() {
        super();
    }


    public PrintMessageCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public PrintMessageCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Prints message to client
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        Client cl = (Client) sender;
        cl.printMessage(this.text, this.from);
    }


    public String getName() {
        return "print";
    }


    public String getDescription() {
        return "Prints a message to client";
    }


    public String getParamsDescription() {
        return "message";
    }
}
