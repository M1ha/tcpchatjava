package com.mycompany.app.client.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import org.json.simple.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Sends text message to clients
 */
public class SendMessageCommand extends ClientAbstractCommand {

    public SendMessageCommand() {
        super();
    }


    public SendMessageCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public SendMessageCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * This method is overridden, as /send command can be ommited.
     *
     * @param cmd  Command string to parse
     * @param from client name, from which message is sent
     * @throws CommandNameException If command name is not send, or command text is not given.
     */
    public void fromString(String cmd, String from) throws CommandNameException {
        Pattern cmdPatt = Pattern.compile("^(@(\\s*[^,\\s]*?\\s*,)*\\s*[^,\\s]*\\s+)?(/send)?\\s*(.+?)$");
        /*
            SendCommand format:
            [@to1, to2, to3] [/cmd] message
            Regexp parts:
            ^(@(\s*[^,\s]*?\s*,)*\s*[^,\s]*\s+)? - [@to1, to2, to3]: all names with @ symbols and spaces at the end,
                if present. All expression is in group 1.
                @(\s*[^,\s]*?\s*,)+ - @to1, to2: til last comma symbol
                \s*[^,\s]*\s+ - to3 : last one name
            (/send)?\s* - [/send]: command if present. All expression is in group 4.
            (.+?) - required message text
                All expression is in group 5.
         */
        Matcher m = cmdPatt.matcher(cmd);
        if (m.matches()) {
            this.parseValuesString(from, m.group(1), m.group(4));
        } else {
            this.inited = false;
            throw new CommandNameException("send");
        }
    }


    public String getName() {
        return "send";
    }


    public String getDescription() {
        return "Sends message to other clients";
    }


    public String getParamsDescription() {
        return "message";
    }
}
