package com.mycompany.app.client.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import org.json.simple.JSONObject;

/**
 * Returns time string from server to client
 */
public class ServerTimeCommand extends ClientAbstractCommand {

    public ServerTimeCommand() {
        super();
    }


    public ServerTimeCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public ServerTimeCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    public String getName() {
        return "serverTime";
    }


    public String getDescription() {
        return "Returns time on server";
    }
}
