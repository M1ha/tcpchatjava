package com.mycompany.app.client.commands;

import java.util.Arrays;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

/**
 * Starts client changing name process.
 * It must be finished by SetClientNameSuccessCommand in order everything works fine.
 */
public class SetClientNameCommand extends ClientAbstractCommand {

    public SetClientNameCommand() {
        super();
    }


    public SetClientNameCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public SetClientNameCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Stops console reading and sends command to server, if connected.
     * Changes local name, if not connected.
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();

        // Try changing client name and send result to client
        Client cl = (Client) sender;
        this.text = this.text.trim();
        if (!Arrays.asList(this.to).contains("Sys") || this.to.length > 1)
            cl.printError("You can't change another client's name", this.from);
        else if (cl.isConnectedToServer()) {
            cl.stopConsoleReading();
            cl.sendCommandToServer(this);
        } else {
            cl.setName(this.text);
            cl.printMessage("Your name is " + this.text + " now", "Sys");
        }
    }


    public String getName() {
        return "setName";
    }


    public String getDescription() {
        return "Sets client name";
    }


    public String getParamsDescription() {
        return "newName";
    }
}
