package com.mycompany.app.client.commands;

import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

/**
 * Is called by server, if client name was changed successfully.
 * Text attribute contains new name.
 */
public class SetClientNameSuccessCommand extends SysAbstractCommand {

    public SetClientNameSuccessCommand() {
        super();
    }


    public SetClientNameSuccessCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public SetClientNameSuccessCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Sets name locally and shows message to client.
     * Previous commands must stop client's input reading in order everything works correctly.
     *
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Try changing client name and send result to client
        Client cl = (Client) sender;
        String name = this.text.trim();
        cl.setName(name);
        cl.printMessage("Now you are '" + name + "'", this.from);
        cl.resumeConsoleReading();
    }


    public String getName() {
        return "setName";
    }


    public String getDescription() {
        return "Sets client name";
    }


    public String getParamsDescription() {
        return "newName";
    }
}
