package com.mycompany.app.client.commands;

import com.mycompany.app.commands.AbstractCommand;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import org.json.simple.JSONObject;

/**
 * Abstract class of client command, which is received from system only (scope = sys)
 * client can't execute this commands.
 */
public abstract class SysAbstractCommand extends AbstractCommand {

    public SysAbstractCommand() {
        super();
    }


    public SysAbstractCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public SysAbstractCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    public String getScope() {
        return "sys";
    }


    public String getParamsDescription() {
        return "";
    }
}
