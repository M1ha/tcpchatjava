package com.mycompany.app.client.commands;

import com.mycompany.app.client.Client;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import org.json.simple.JSONObject;

/**
 * Prints client name
 */
public class WhoAmICommand extends ClientAbstractCommand {

    public WhoAmICommand() {
        super();
    }


    public WhoAmICommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public WhoAmICommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Prints client's name
     * @param sender Client instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Print name here
        Client cl = (Client) sender;
        this.sendLocalString(cl, "Your name is '" + cl.getName() + "'");
    }


    public String getName() {
        return "whoAmI";
    }


    public String getDescription() {
        return "Returns your name";
    }
}
