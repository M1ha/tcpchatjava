package com.mycompany.app.commands;

import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;

/**
 * This class provides basic operations on AbstractCommand for using by CommandFactory and external services
 * Realises Command, Prototype OOP patterns
 */
public abstract class AbstractCommand {
    protected String from, text;
    protected String[] to;
    protected boolean inited;


    /**
     * Create an empty, not initialized command
     */
    public AbstractCommand() {
        this.from = "";
        this.text = "";
        this.inited = false;
    }


    /**
     * Parse command string
     *
     * @param cmd  Command string to parse
     * @param from Command sender name (client name or Sys)
     * @throws CommandNameException cmd "name" doesn't correspond to this command's getName() result
     */
    public AbstractCommand(String cmd, String from) throws CommandNameException {
        this.fromString(cmd, from);
    }


    /**
     * Parse command JSONObject
     *
     * @param cmd Command JSONObject with from (string), to (array of strings), text (string) and name (string) fields
     * @throws CommandNameException   cmd "name" doesn't correspond to this command's getName() result
     * @throws CommandFormatException Some field is not found in cmd or have wrong format
     */
    public AbstractCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        this.fromJSON(cmd);
    }


    /**
     * Represents command as a string in format @to /command text
     *
     * @return A string in format @to /command text. If command is not initialized returns null. It's better to raise
     * {@link CommandNotInitializedException}, but for Object.toString() compatibility it's not raised.
     */
    public String toString() {
        if (!this.isInitizlized())
            return null;
        String result = "";
        if (this.to != null) {
            result += "@";
            for (String name : this.to) {
                result += name + ", ";
            }

            if (result.length() > 1) // No names
                result = result.substring(0, result.length() - 2);
            result += " ";
        }

        result += "/" + this.getName() + " ";

        result += text;
        return result;
    }


    /**
     * Parses string values, got in fromString method to this object attributes.
     * It's useful for overridden fromString() method.
     *
     * @param from client name, from which message is sent
     * @param to   A string of client names or Sys, divided by comma, to which command is sent. Each client can be prefixed
     *             with @ symbol which will be removed.
     * @param text Text parameter for this command
     */
    protected void parseValuesString(String from, String to, String text) {
        HashSet<String> toSet = new HashSet<String>();
        if (to == null)
            toSet.add("Sys");
        else {
            this.to = to.split("\\s*,\\s*");
            for (String name : to.split("\\s*,\\s*")) {
                name = name.trim();
                if (name.charAt(0) == '@')
                    name = name.substring(1);
                if (name.length() > 0)
                    toSet.add(name);
            }
            if (toSet.size() == 0)
                toSet.add("Sys");
        }
        this.to = toSet.toArray(new String[0]);

        if (text == null)
            this.text = "";
        else
            this.text = text;
        this.from = from;
    }


    /**
     * Parses a command line, got from client to this object's attributes
     *
     * @param cmd  Command string to parse
     * @param from client name, from which message is sent
     * @throws CommandNameException cmd "name" doesn't correspond to this command's getName() result
     */
    public void fromString(String cmd, String from) throws CommandNameException {
        Pattern cmdPatt = Pattern.compile("^(@(\\s*[^,\\s]*?\\s*,)*\\s*[^,\\s]*\\s+)?(/([^\\s]+?))?(\\s+(.*?))?$");
        /*
            AbstractCommand format:
            [@to1, to2, to3] [/cmd] message
            Regexp parts:
            ^(@(\s*[^,\s]*?\s*,)*\s*[^,\s]*\s+)? - [@to1, to2, to3]: all names with @ symbols and spaces at the end,
                if present. All expression is in group 1.
                @(\s*[^,\s]*?\s*,)+ - @to1, to2: til last comma symbol
                \s*[^,\s]*\s+ - to3 : last one name
            (/([^\s]+?))?\s* - [/cmd]: command if present. All expression is in group 4.
            (\s+(.*?))? - message: command arguments or broadcast message text if no command is given.
                All expression is in group 5.
         */
        Matcher m = cmdPatt.matcher(cmd);
        if (m.matches()) {
            if (m.group(3) == null || !m.group(4).toLowerCase().equals(this.getName().toLowerCase()))
                throw new CommandNameException(m.group(4));
            this.parseValuesString(from, m.group(1), m.group(5));
        } else
            this.inited = false;
    }


    /**
     * Initializes object from raw given attributes.
     *
     * @param from client name, from which message is sent
     * @param to   A set of client names or Sys to whoom command is sent
     * @param text Text message of the command
     */
    public void fromRawData(String from, String[] to, String text) {
        this.from = from;
        this.to = to;
        this.text = text;
        this.inited = true;
    }


    /**
     * A realization of fromRawData method for only one receiver.
     * It's a very common issue.
     *
     * @param from client name, from which message is sent
     * @param to   A single client name to send command to
     * @param text Text message of the command
     */
    public void fromRawData(String from, String to, String text) {
        this.from = from;
        this.to = new String[1];
        this.to[0] = to;
        this.text = text;
        this.inited = true;
    }


    /**
     * Parse command JSONObject
     *
     * @param cmd Command JSONObject with from (string), to (array of strings), text (string) and name (string) fields
     * @throws CommandNameException   cmd "name" doesn't correspond to this command's getName() result
     * @throws CommandFormatException Some field is not found in cmd or have wrong format
     */
    public void fromJSON(JSONObject cmd) throws CommandNameException, CommandFormatException {
        this.inited = false;
        if (!cmd.containsKey("name"))
            throw new CommandFormatException("name");
        if (!cmd.containsKey("to"))
            throw new CommandFormatException("to");
        if (!cmd.containsKey("from"))
            throw new CommandFormatException("from");
        if (!cmd.containsKey("text"))
            throw new CommandFormatException("text");

        String cmdStr = cmd.get("name").toString().toLowerCase();
        if (!cmdStr.equals(this.getName().toLowerCase()))
            throw new CommandNameException(cmdStr);

        this.text = cmd.get("text").toString();
        this.from = cmd.get("from").toString();
        JSONArray arr = (JSONArray) cmd.get("to");
        this.to = new String[arr.size()];
        for (int i = 0; i < arr.size(); i++)
            this.to[i] = arr.get(i).toString();
        this.inited = true;
    }


    /**
     * Converts this object to JSONObject
     *
     * @return JSONObject with from (string), to (array of strings), text (string) and name (string) fields
     * @throws CommandNotInitializedException If command was not initialized correctly before this method.
     */
    public JSONObject toJSON() throws CommandNotInitializedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        JSONObject obj = new JSONObject();
        obj.put("from", this.from);
        obj.put("name", this.getName().toLowerCase());
        obj.put("text", this.text);
        JSONArray arr = new JSONArray();
        arr.addAll(Arrays.asList(this.to));
        obj.put("to", arr);
        return obj;
    }


    /**
     * Tries to parse jsonCommand and executes fromJSON() method on result if succeeded.
     *
     * @param jsonCommand JSON parsable string
     * @throws CommandNameException   cmd "name" doesn't correspond to this command's getName() result
     * @throws CommandFormatException Some field is not found in cmd or have wrong format
     * @throws ParseException         JSON parser failed to parse jsonCommand string
     */
    public void fromJSONString(String jsonCommand)
            throws CommandNameException, CommandFormatException, ParseException {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(jsonCommand);
        this.fromJSON(obj);
    }


    /**
     * Converts this object to JSONObject with toJSON() method and returns it's string representation.
     *
     * @return A serialized JSONObject, representing this command
     */
    public String toJSONString() throws CommandNotInitializedException {
        JSONObject obj = this.toJSON();
        return obj.toJSONString();
    }


    /**
     * Returns text of current command
     *
     * @return A serialized JSONObject, representing this command
     */
    public String getText() throws CommandNotInitializedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        return this.text;
    }


    /**
     * Returns a set of client names this command is sent to
     *
     * @return An array of client names this command is sent to
     */
    public String[] getTo() throws CommandNotInitializedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        return this.to;
    }


    /**
     * Returns a client name this command is sent by
     *
     * @return A client name this command is sent by
     */
    public String getFrom() throws CommandNotInitializedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        return this.from;
    }


    /**
     * Test, if current command was initialized successfully
     *
     * @return Boolean
     */
    public boolean isInitizlized() {
        return this.inited;
    }


    /**
     * Realizes prototype OOP pattern. Creates and returns a copy of current command.
     *
     * @return A copy of command
     */
    public AbstractCommand clone() throws CloneNotSupportedException {
        try {
            AbstractCommand cmd = this.getClass().newInstance();
            cmd.fromRawData(this.from, this.to, this.text);
            return cmd;
        } catch (Exception e) {
            throw new CloneNotSupportedException();
        }
    }


    /**
     * Command name. Must be unique inside scope.
     * This name is used by CommandFactory class, when clients gives /command in input.
     * Command name comparison is case insensitive!!!
     *
     * @return Command name
     */
    public abstract String getName();

    /**
     * Command short description. It is used for creating help message.
     *
     * @return Command description
     */
    public abstract String getDescription();

    /**
     * Description of command parameters. It is used for creating help message.
     *
     * @return Command parameters description
     */
    public abstract String getParamsDescription();

    /**
     * Returns command scope.
     * Scope helps to extract package commands easier in fabric by scope list.
     *
     * @return Scope string
     */
    public abstract String getScope();

    /**
     * Realizes command OOP pattern. Command execution method.
     *
     * @param sender An object to work with. It can be client or server in TcpChat
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public abstract void execute(Object sender) throws CommandNotInitializedException, InterruptedException;
}
