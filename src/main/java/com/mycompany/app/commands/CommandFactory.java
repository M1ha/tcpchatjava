package com.mycompany.app.commands;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Arrays;
import java.util.List;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.DuplicatedCommandException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.reflections.Reflections;

/**
 * This class represents a factory for creating Commands from text messages
 * Realises Factory OOP pattern
 */
public class CommandFactory {
    private HashMap<String, AbstractCommand> data;


    /**
     * Initializes factory without commands
     */
    public CommandFactory() {
        this.data = new HashMap<String, AbstractCommand>();
    }


    /**
     * Initializes factory, adding all commands from package which have scope from scopeList
     * A command names list inside one factory must be unique!!!
     *
     * @param packageName A package canonical name to search AbstractCommand child classes in
     * @param scopeList   Filters commands, found in package by their scopes
     */
    public CommandFactory(String packageName, String[] scopeList) {
        this.data = new HashMap<String, AbstractCommand>();
        this.registerCommandPackage(packageName, scopeList);
    }


    /**
     * Adds new AbstractCommand child class to factory by it's class.
     *
     * @param cmd Command ClassInfo
     * @throws DuplicatedCommandException If command already exists in factory
     */
    public void registerCommand(Class<? extends AbstractCommand> cmd) throws DuplicatedCommandException {
        try {
            this.registerCommand(cmd.newInstance());
        } catch (IllegalAccessException e) {
            // Ignore class
        } catch (InstantiationException e) {
            // Ignore class
        }
    }


    /**
     * Adds new AbstractCommand realization to factory.
     *
     * @param cmd AbstractCommand child instance
     * @throws DuplicatedCommandException If command already exists in factory
     */
    public void registerCommand(AbstractCommand cmd) throws DuplicatedCommandException {
        // Command name must be unique in single factory
        String name = cmd.getName().toLowerCase();
        if (this.data.containsKey(name))
            throw new DuplicatedCommandException(name);
        this.data.put(name, cmd);
    }


    /**
     * Adds all commands from package which have scope from scopeList
     * A command names list inside one factory must be unique!!!
     *
     * @param name      A package canonical name to search AbstractCommand child classes in
     * @param scopeList Filters commands, found in package by their scopes
     */
    public void registerCommandPackage(String name, String[] scopeList) {
        Reflections reflections = new Reflections(name);

        Set<Class<? extends AbstractCommand>> cmdClasses = reflections.getSubTypesOf(AbstractCommand.class);
        List<String> scopeStringList = Arrays.asList(scopeList);
        boolean allScopes = scopeStringList.contains("*");
        for (Class<? extends AbstractCommand> cls : cmdClasses) {
            try {
                AbstractCommand inst = cls.newInstance();
                if (allScopes || scopeStringList.contains(inst.getScope()))
                    try {
                        this.registerCommand(cls);
                    } catch (DuplicatedCommandException e) {
                        // It can never be, file names in package are unique
                    }
            } catch (IllegalAccessException e) {
                // Ignore class
            } catch (InstantiationException e) {
                // Ignore class
            }
        }
    }


    /**
     * Returns a set of command names, registered in factory
     */
    public Set<String> getCommandNamesList() {
        return this.data.keySet();
    }


    /**
     * Returns a Collection of AbstractCommand instances, registered in factory
     */
    public Collection<AbstractCommand> getCommandList() {
        return this.data.values();
    }


    /**
     * Gets command description by it's name
     *
     * @param cmdName A name of command, registered in factory
     * @return A string description of a command
     * @throws CommandNameException If command is not registered in factory
     */
    public String getCommandDescription(String cmdName) throws CommandNameException {
        if (!this.data.containsKey(cmdName))
            throw new CommandNameException(cmdName);
        else
            return this.data.get(cmdName).getDescription();
    }


    /**
     * Tries to get command from client's string
     *
     * @param command A command string from client input
     * @param from    A name of client, who sends a command
     * @return An AbstractCommand child class instance
     * @throws CommandNameException       If command is not registered in factory
     * @throws CloneNotSupportedException If clone is not supported by the executing system
     */
    public AbstractCommand getCommand(String command, String from)
            throws CommandNameException, CloneNotSupportedException {
        Pattern cmdPatt = Pattern.compile("^.*?/(\\S+).*$");
        Matcher m = cmdPatt.matcher(command);
        String name;
        if (!m.matches() || m.group(1) == null) {
            // Default command
            name = "send";
        } else
            name = m.group(1).toLowerCase();

        if (!this.data.containsKey(name))
            throw new CommandNameException(name);

        AbstractCommand cmd = this.data.get(name).clone();
        cmd.fromString(command, from);
        return cmd;
    }


    /**
     * Tries to get command from serialized JSONObject
     *
     * @param jsonCommand Serialized JSONObject to parse
     * @return An AbstractCommand child class instance
     * @throws CommandNameException       If command is not registered in factory
     * @throws CommandFormatException     If parsed object has wrong format
     * @throws CloneNotSupportedException If clone is not supported by the executing system
     * @throws ParseException             If command can not be parsed
     */
    public AbstractCommand getCommandFromJSONString(String jsonCommand)
            throws CommandNameException, CommandFormatException, CloneNotSupportedException, ParseException {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(jsonCommand);
        return this.getCommand(obj);
    }


    /**
     * Tries to get command from JSONObject
     *
     * @param command JSONObject to get data from
     * @return An AbstractCommand child class instance
     * @throws CommandNameException       If command is not registered in factory
     * @throws CommandFormatException     If parsed object has wrong format
     * @throws CloneNotSupportedException If clone is not supported by the executing system
     */
    public AbstractCommand getCommand(JSONObject command)
            throws CommandNameException, CommandFormatException, CloneNotSupportedException {
        String name = command.get("name").toString();
        if (!this.data.containsKey(name))
            throw new CommandNameException(name);

        AbstractCommand cmd = this.data.get(name).clone();
        cmd.fromJSON(command);
        return cmd;
    }
}
