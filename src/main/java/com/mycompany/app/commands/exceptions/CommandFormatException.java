package com.mycompany.app.commands.exceptions;

/**
 * This exception is raised, when JSONObject command doesn't contain correct initialization fieldss
 */
public class CommandFormatException extends Exception {
    private String paramName;


    public CommandFormatException(String name) {
        super();
        this.paramName = name;
    }


    public String toString() {
        return "No key in JSON '" + this.paramName + "'";
    }
}