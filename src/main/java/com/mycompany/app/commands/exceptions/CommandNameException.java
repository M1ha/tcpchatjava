package com.mycompany.app.commands.exceptions;

/**
 * This exception is raised, when command name is wrong while parsing, or is absent in CommandFactory
 */
public class CommandNameException extends Exception {
    private String cmdName;


    public CommandNameException(String name) {
        super();
        this.cmdName = name;
    }


    public String toString() {
        return "Invalid command name '" + this.cmdName + "'";
    }
}
