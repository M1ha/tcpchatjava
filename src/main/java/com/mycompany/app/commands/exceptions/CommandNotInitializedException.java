package com.mycompany.app.commands.exceptions;

/**
 * This exception is raised, when some AbstractCommand child instance method where called before command was initialized correctly
 */
public class CommandNotInitializedException extends Exception {
    public CommandNotInitializedException() {
        super();
    }


    public String toString() {
        return "Command was not initialized properly";
    }
}