package com.mycompany.app.commands.exceptions;

/**
 * This exception is raised, when duplicate key is added to CommandFactory
 */
public class DuplicatedCommandException extends Exception {
    private String cmdName;


    public DuplicatedCommandException(String name) {
        super();
        this.cmdName = name;
    }


    public String toString() {
        return "Duplicate command '" + this.cmdName + "' registration";
    }
}
