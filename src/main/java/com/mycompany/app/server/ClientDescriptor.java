package com.mycompany.app.server;

import java.util.Arrays;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.net.Socket;

/**
 * This class describes client for server
 */
class ClientDescriptor {
    protected String name;
    private Socket sock;
    protected CircularFifoQueue<String> messageHistory;


    /**
     * Initializes new client
     *
     * @param name Client name
     * @param s    Socket, through which communication is done
     */
    ClientDescriptor(String name, Socket s, int historySize) {
        this.name = name;
        this.sock = s;
        this.messageHistory = new CircularFifoQueue<String>(historySize);
    }


    /**
     * Returns client name
     *
     * @return Client name
     */
    public String getName() {
        return this.name;
    }


    /**
     * Sets client name
     *
     * @param name New name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Returns socket to communicate through
     *
     * @return Socket instance
     */
    public Socket getSocket() {
        return this.sock;
    }


    /**
     * Sets new socket for this client
     *
     * @param s Socket instance to set
     */
    public void setSocket(Socket s) {
        this.sock = s;
    }


    /**
     * Writes a message to history
     *
     * @param msg  Message to write
     * @param from Client name from which message came
     */
    public synchronized void writeMessageToHistory(String msg, String from) {
        if (this.getName().equals(from))
            this.messageHistory.add("You: " + msg);
        else
            this.messageHistory.add(from + ": " + msg);
    }


    /**
     * Returns message history. Each message is prefixed by sender
     *
     * @return Array of history strings
     */
    public synchronized String[] getMessageHistory() {
        String result[] = new String[this.messageHistory.size()];
        result = this.messageHistory.toArray(result);
        return result;
    }


    /**
     * Test if client is connected to server at the moment
     *
     * @return boolean
     */
    public boolean isConnected() {
        return !this.sock.isClosed();
    }


    /**
     * Updates data from given client descriptor (in order to merge on renaming)
     *
     * @param cl     ClientDescriptor to get data from
     * @param append If True, appends cl history to current. If false - prepends.
     */
    void updateHistory(ClientDescriptor cl, boolean append) {
        if (append) {
            this.messageHistory.addAll(Arrays.asList(cl.getMessageHistory()));
        } else {
            String[] currentHist = this.getMessageHistory();
            this.messageHistory.clear();
            this.messageHistory.addAll(Arrays.asList(cl.getMessageHistory()));
            this.messageHistory.addAll(Arrays.asList(currentHist));
        }
    }
}
