package com.mycompany.app.server;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;

import com.mycompany.app.client.commands.PrintErrorMessageCommand;
import com.mycompany.app.client.commands.SetClientNameSuccessCommand;
import com.mycompany.app.commands.AbstractCommand;
import com.mycompany.app.commands.CommandFactory;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;

public class Server {
    protected Integer clientNameIncrement;
    protected HashMap<String, ClientDescriptor> clients;
    protected ReentrantReadWriteLock clientLock;
    protected Logger log;
    protected CommandFactory serverCommandFactory;
    protected int historySize;


    /**
     * Creates server.
     * Initializes TCP-server socket, server CommandFactory.
     * Starts server receive commands cycle
     * Exceptions during init process are not raise, they are logged.
     *
     * @param host Ip address to listen on
     * @param port Tcp port to listen on
     */
    public Server(String host, Integer port, int historySize) {
        this.log = Logger.getLogger(Server.class.getName());
        this.clientNameIncrement = 0;
        this.clients = new HashMap<String, ClientDescriptor>();
        this.clientLock = new ReentrantReadWriteLock();
        this.historySize = historySize;
        String[] scopeList = {"server"};
        this.serverCommandFactory = new CommandFactory("com.mycompany.app.server.commands", scopeList);
        try {
            ServerSocket server;
            if (!host.equals("ALL"))
                server = new ServerSocket(port, 0, InetAddress.getByName(host));
            else
                server = new ServerSocket(port);
            this.log.info("Start listening on " + host + ":" + port.toString());


            while (true) {
                Socket s = server.accept();
                ServerThread t = new ServerThread(s, this);
                t.setDaemon(true);
                t.setPriority(Thread.NORM_PRIORITY);
                t.start();
            }
        } catch (Exception e) {
            this.log.log(Level.SEVERE, e.toString(), e);
        }
    }


    /**
     * Returns server logger object
     *
     * @return Logger instance
     */
    public Logger getLog() {
        return this.log;
    }


    /**
     * Adds new client
     *
     * @param name Client name
     * @param sock Client socket
     * @return ClientDescriptor instance
     */
    public ClientDescriptor addClient(String name, Socket sock) {
        this.clientLock.readLock().lock();
        ClientDescriptor d;
        if (this.clients.containsKey(name) && !this.clients.get(name).isConnected()) {
            d = this.clients.get(name);
            d.setSocket(sock);
            this.clientLock.readLock().unlock();
        } else {
            d = new ClientDescriptor(name, sock, this.historySize);
            this.clientLock.readLock().unlock();
            this.clientLock.writeLock().lock();
            this.clients.put(name, d);
            this.clientLock.writeLock().unlock();
        }
        return d;
    }


    /**
     * Disconnects client from server, saving his ClientDescriptor in memory
     *
     * @param cl ClientDescriptorInstance
     */
    public void disconnectClient(ClientDescriptor cl) {
        if (cl.isConnected()) {
            try {
                Socket s = cl.getSocket();
                s.close();
            } catch (IOException e) {
                // Do nothing
            }
        }
    }


    /**
     * Returns Server command factory
     *
     * @return CommandFactory instance
     */
    public CommandFactory getServerCommandFactory() {
        return this.serverCommandFactory;
    }


    /**
     * Sends command to given clients
     *
     * @param cmd      AbstractCommand child instance to send
     * @param nameList A list of client names to send to
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     */
    public void sendCommandToClients(AbstractCommand cmd, String[] nameList)
            throws CommandNotInitializedException {

        for (String name : nameList) {
            this.clientLock.readLock().lock();
            ClientDescriptor cl = this.clients.get(name);
            this.clientLock.readLock().unlock();
            if (cl == null) {
                PrintErrorMessageCommand errCmd = new PrintErrorMessageCommand();
                errCmd.fromRawData("Sys", cmd.getFrom(), "Sorry, name '" + name + "' doesn't exist! " +
                        "Press /clients to get connected clients list");
                this.sendCommandToClient(errCmd, cmd.getFrom());
            } else {
                try {
                    OutputStream outStream = cl.getSocket().getOutputStream();
                    outStream.write((cmd.toJSONString() + "\0").getBytes());
                } catch (IOException e) {
                    this.log.warning("Error writing to socket " + cl.getName());
                    this.disconnectClient(cl);
                }
            }
        }
    }


    /**
     * Sends command to single client. Common case.
     *
     * @param cmd  AbstractCommand child instance to send
     * @param name Client name to send command to
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     */
    public void sendCommandToClient(AbstractCommand cmd, String name) throws CommandNotInitializedException {
        String[] nameList = {name};
        this.sendCommandToClients(cmd, nameList);
    }


    /**
     * Returns a set of connected to server client names
     *
     * @return A set of connected to server client names
     */
    public String[] getClientNameSet() {
        HashSet<String> res = new HashSet<String>();
        this.clientLock.readLock().lock();
        for (ClientDescriptor cl : this.clients.values())
            if (cl.isConnected())
                res.add(cl.getName());
        this.clientLock.readLock().unlock();
        return res.toArray(new String[0]);
    }


    /**
     * Tries setting client name on server.
     * If other client has this name, sends error to client.
     * Else sends SetClientNameSuccessCommand to client.
     *
     * @param from Old client name
     * @param to   New client name
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     */
    public void changeClientName(String from, String to) throws CommandNotInitializedException {
        String resName = to;
        this.clientLock.readLock().lock();
        if (to.equals("Client") || to.equals("Sys") ||
                !from.equals(to) && this.clients.containsKey(to) && this.clients.get(to).isConnected()) {
            this.clientLock.readLock().unlock();
            PrintErrorMessageCommand cmd = new PrintErrorMessageCommand();
            cmd.fromRawData("Sys", from, "Sorry, name '" + to + "' is busy!");
            this.sendCommandToClient(cmd, from);
            resName = from;
        } else if (!from.equals(to)) {
            if (!this.clients.containsKey(from)) {
                this.clientLock.readLock().unlock();
                this.log.warning("Wrong client name " + from);
            } else {
                ClientDescriptor fromDesc = this.clients.get(from);
                this.clientLock.readLock().unlock();
                this.clientLock.writeLock().lock();
                if (this.clients.containsKey(to)) {
                    ClientDescriptor toDesc = this.clients.get(to);
                    toDesc.updateHistory(fromDesc, true);
                    toDesc.setSocket(fromDesc.getSocket());
                } else {
                    fromDesc.setName(to);
                    this.clients.put(to, fromDesc);
                }
                this.clients.remove(from);
                this.clientLock.writeLock().unlock();
            }
        }
        SetClientNameSuccessCommand cmd = new SetClientNameSuccessCommand();
        cmd.fromRawData("Sys", resName, resName);
        this.sendCommandToClient(cmd, resName);
    }


    /**
     * Generates new client name by counter
     *
     * @return New client name
     */
    public synchronized String generateClientName() {
        String name = "Client#";
        while (this.clients.containsKey(name + this.clientNameIncrement.toString()))
            this.clientNameIncrement++;
        name += this.clientNameIncrement.toString();
        this.clientNameIncrement++;
        return name;
    }


    /**
     * Returns client command history by his name
     *
     * @param name Client name
     * @return An array of Strings
     */
    public String[] getClientMessageHistory(String name) {
        this.clientLock.readLock().lock();
        String[] res;
        if (!this.clients.containsKey(name)) {
            this.log.warning("Wrong client name " + name);
            res = new String[0];
        } else {
            ClientDescriptor d = this.clients.get(name);
            res = d.getMessageHistory();
        }
        this.clientLock.readLock().unlock();
        return res;
    }


    /**
     * Returns client command history by his name
     *
     * @param name Client name
     * @param msg  Message for history
     * @param from Client name, from which message came
     */
    public void writeToClientMessageHistory(String name, String msg, String from) {
        this.clientLock.readLock().lock();
        if (!this.clients.containsKey(name)) {
            this.log.warning("Wrong client name " + name);
        } else {
            ClientDescriptor d = this.clients.get(name);
            d.writeMessageToHistory(msg, from);
        }
        this.clientLock.readLock().unlock();
    }
}
