package com.mycompany.app.server;

import java.io.IOException;
import java.net.Socket;
import java.io.InputStream;
import java.net.SocketException;
import java.util.logging.Level;

import com.mycompany.app.client.commands.ConnectSuccessCommand;
import com.mycompany.app.commands.AbstractCommand;
import com.mycompany.app.commands.CommandFactory;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;

/**
 * A server thread, working with single client
 */
class ServerThread extends Thread {
    private Server server;
    private Socket sock;
    private String readBuffer;


    /**
     * Initializes thread
     *
     * @param s      Socket to get client commands from
     * @param server Server to work with
     */
    ServerThread(Socket s, Server server) {
        this.sock = s;
        this.server = server;
        this.readBuffer = "";
    }


    /**
     * Initializes connection with client, then sets his name.
     * After that starts reading and executing commands from client.
     * If any exception is raised, logs it and continues working.
     */
    public void run() {
        ClientDescriptor client;
        String name = this.server.generateClientName();
        client = this.server.addClient(name, this.sock);
        ConnectSuccessCommand sCmd = new ConnectSuccessCommand();
        sCmd.fromRawData(name, "Sys", name);
        try {
            this.server.sendCommandToClient(sCmd, name);
        } catch (CommandNotInitializedException e) {
            this.server.getLog().log(Level.SEVERE, e.toString(), e);
        }
        while (client.isConnected()) {
            try {
                // Read command. It may come partially, or multiple commands a time
                InputStream inpStream = client.getSocket().getInputStream();
                int splitIndex = -1;
                int readCnt = 0;
                while (splitIndex < 0 && readCnt >= 0) {
                    byte buf[] = new byte[64 * 1024];
                    readCnt = inpStream.read(buf);
                    if (readCnt >= 0) {
                        this.readBuffer += new String(buf, 0, readCnt);
                        splitIndex = this.readBuffer.indexOf('\0');
                    } else {
                        // Client down
                    }
                }
                if (splitIndex >= 0) {
                    String data = this.readBuffer.substring(0, splitIndex);
                    this.readBuffer = this.readBuffer.substring(splitIndex + 1);

                    // Convert data to command
                    CommandFactory cmdFact = this.server.getServerCommandFactory();
                    AbstractCommand cmd = cmdFact.getCommandFromJSONString(data);

                    // Execute command
                    cmd.execute(this.server);
                } else {
                    // Client down
                    this.server.disconnectClient(client);
                }
            } catch (SocketException e) {
                this.server.disconnectClient(client);
                try {
                    client.getSocket().close();
                } catch (IOException exception) {
                    // Ignore, closed already
                }
            } catch (Exception e) {
                this.server.getLog().log(Level.SEVERE, e.toString(), e);
            }
        }
    }
}
