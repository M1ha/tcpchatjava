package com.mycompany.app.server.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import com.mycompany.app.server.Server;
import org.json.simple.JSONObject;

/**
 * Prints a list of clients to receivers
 */
public class GetClientListCommand extends ServerAbstractCommand {

    public GetClientListCommand() {
        super();
    }


    public GetClientListCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public GetClientListCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Prints a list of clients to receivers
     * @param sender Server instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Send command to print client list
        Server s = (Server) sender;
        String[] clients = s.getClientNameSet();
        String text;
        if (clients.length == 0)
            text = "There are no clients";
        else
            text = "clients: " + String.join(", ", clients);
        this.sendStringToClients(s, text);
    }


    public String getName() {
        return "clients";
    }


    public String getDescription() {
        return "Returns a list of client names, connected to server";
    }
}
