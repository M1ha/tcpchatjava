package com.mycompany.app.server.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import com.mycompany.app.server.Server;
import org.json.simple.JSONObject;

/**
 * Prints number of clients, connected to server to receivers
 */
public class GetClientsCountCommand extends ServerAbstractCommand {

    public GetClientsCountCommand() {
        super();
    }


    public GetClientsCountCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public GetClientsCountCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Prints number of connected clients to receivers
     *
     * @param sender Server instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Get number of clients and print it to client
        Server s = (Server) sender;
        Integer clientNumber = s.getClientNameSet().length;
        this.sendStringToClients(s, "Number of clients: " + clientNumber.toString());
    }


    public String getName() {
        return "clientsCount";
    }


    public String getDescription() {
        return "Returns number of clients, connected to server";
    }
}
