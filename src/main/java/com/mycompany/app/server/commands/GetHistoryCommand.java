package com.mycompany.app.server.commands;

import com.mycompany.app.client.commands.PrintErrorMessageCommand;
import com.mycompany.app.client.commands.PrintMessageCommand;
import com.mycompany.app.commands.AbstractCommand;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import com.mycompany.app.server.Server;
import org.json.simple.JSONObject;

/**
 * Prints history of commands done by the user
 */
public class GetHistoryCommand extends ServerAbstractCommand {

    public GetHistoryCommand() {
        super();
    }


    public GetHistoryCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public GetHistoryCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Prints history of commands done by the user
     *
     * @param sender Server instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Get number of clients and print it to client
        Server s = (Server) sender;
        String hist[] = s.getClientMessageHistory(this.from);
        String strSize = this.text.trim();
        try {
            String answer = "Chat history: \n";
            int requestedSize = hist.length;
            if (strSize.length() != 0)
                requestedSize = new Integer(strSize);

            for (int i = 0; i < hist.length && i < requestedSize; i++)
                answer += hist[i] + "\n";

            PrintMessageCommand cmd = new PrintMessageCommand();
            cmd.fromRawData("Sys", this.from, answer);
            s.sendCommandToClient(cmd, this.from);
        } catch (NumberFormatException e) {
            PrintErrorMessageCommand cmd = new PrintErrorMessageCommand();
            cmd.fromRawData("Sys", this.from, "History size must be integer, not '" + this.text + "'!");
            s.sendCommandToClient(cmd, this.from);
        }
    }


    public String getName() {
        return "history";
    }


    public String getDescription() {
        return "Returns command history for current client";
    }


    public String getParamsDescription() {
        return "size";
    }
}
