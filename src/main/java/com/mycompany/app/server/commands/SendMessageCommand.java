package com.mycompany.app.server.commands;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import com.mycompany.app.server.Server;
import org.json.simple.JSONObject;

/**
 * This command sends messages to receivers
 */
public class SendMessageCommand extends ServerAbstractCommand {

    public SendMessageCommand() {
        super();
    }


    public SendMessageCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public SendMessageCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Sends message to receivers
     *
     * @param sender Server instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Send message to clients here
        Server s = (Server) sender;
        String[] receivers = this.getReceivers(s);
        for (String name : receivers)
            s.writeToClientMessageHistory(name, this.text, this.from);
        s.writeToClientMessageHistory(this.from, this.text, this.from);

        this.sendStringToClients(s, this.getText());
    }


    /**
     * Overrides getReceivers, excluding sending users from receivers
     *
     * @param server Server instance to work with
     * @return A set of receiver names
     */
    protected String[] getReceivers(Server server) {
        String[] receivers = super.getReceivers(server);
        List<String> l = new ArrayList<String>(Arrays.asList(receivers));
        l.remove(this.from);
        return  l.toArray(new String[0]);
    }


    public String getName() {
        return "send";
    }


    public String getDescription() {
        return "Sends message to other clients";
    }
}
