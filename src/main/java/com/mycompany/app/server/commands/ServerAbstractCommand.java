package com.mycompany.app.server.commands;

import com.mycompany.app.client.commands.PrintMessageCommand;
import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import com.mycompany.app.server.Server;
import com.mycompany.app.commands.AbstractCommand;
import com.mycompany.app.commands.exceptions.CommandNameException;
import org.json.simple.JSONObject;

import java.util.Arrays;


/**
 * An abstract class for any server command (scope = server).
 */
public abstract class ServerAbstractCommand extends AbstractCommand {

    public ServerAbstractCommand() {
        super();
    }


    public ServerAbstractCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public ServerAbstractCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    public String getScope() {
        return "server";
    }


    /**
     * Forms a set of receivers of current message
     *
     * @param server Server instance to work with
     * @return A set of receiver names
     */
    protected String[] getReceivers(Server server) {
        String[] receivers = this.to;
        if (Arrays.asList(receivers).contains("Sys"))
            receivers = server.getClientNameSet();

        return receivers;
    }

    /**
     * Sends a given string to clients in attribute to. If Sys is in receivers, sends message to every client, except sender.
     *
     * @param server Server instance to work with
     * @param data   Data string to send
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    protected void sendStringToClients(Server server, String data)
            throws CommandNotInitializedException, InterruptedException {
        String[] receivers = this.getReceivers(server);
        if (receivers.length > 0) {
            PrintMessageCommand cmd = new PrintMessageCommand();
            cmd.fromRawData(this.from, receivers, data);
            server.sendCommandToClients(cmd, receivers);
        }
    }


    public String getParamsDescription() {
        return "";
    }

}
