package com.mycompany.app.server.commands;

import java.util.Date;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import com.mycompany.app.server.Server;
import org.json.simple.JSONObject;

/**
 * Gets server time string and sends it to receivers.
 */
public class ServerTimeCommand extends ServerAbstractCommand {

    public ServerTimeCommand() {
        super();
    }


    public ServerTimeCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public ServerTimeCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Gets server time string and sends it to receivers.
     *
     * @param sender Server instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Send command to print server time
        Server s = (Server) sender;
        Date now = new Date();
        this.sendStringToClients(s, now.toString());
    }


    public String getName() {
        return "serverTime";
    }


    public String getDescription() {
        return "Returns time on server";
    }
}
