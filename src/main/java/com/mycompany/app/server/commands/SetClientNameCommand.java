package com.mycompany.app.server.commands;

import com.mycompany.app.commands.exceptions.CommandFormatException;
import com.mycompany.app.commands.exceptions.CommandNameException;
import com.mycompany.app.commands.exceptions.CommandNotInitializedException;
import com.mycompany.app.server.Server;
import org.json.simple.JSONObject;

/**
 * Tries setting client name on server.
 * If other client has this name, sends error to client.
 * Else sends SetClientNameSuccessCommand to client.
 */
public class SetClientNameCommand extends ServerAbstractCommand {

    public SetClientNameCommand() {
        super();
    }


    public SetClientNameCommand(String cmd, String from) throws CommandNameException {
        super(cmd, from);
    }


    public SetClientNameCommand(JSONObject cmd) throws CommandNameException, CommandFormatException {
        super(cmd);
    }


    /**
     * Tries setting client name on server.
     * If other client has this name, sends error to client.
     * Else sends SetClientNameSuccessCommand to client.
     *
     * @param sender Server instance
     * @throws CommandNotInitializedException If command was not initialized correctly before using
     * @throws InterruptedException           If some sender lock was interrupted
     */
    public void execute(Object sender) throws CommandNotInitializedException, InterruptedException {
        if (!this.isInitizlized())
            throw new CommandNotInitializedException();
        // Try changing client name and send result to client
        Server s = (Server) sender;
        s.changeClientName(this.from, this.text.trim());
    }


    public String getName() {
        return "setName";
    }


    public String getDescription() {
        return "Sets client name";
    }
}
